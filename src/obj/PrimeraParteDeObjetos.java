package obj;

import java.awt.*;

public class PrimeraParteDeObjetos {

	// 1. La función debe devolver la distancia entre los puntos `p1` y `p2`.
	public static double distancia(Point p1, Point p2) {
		return 0;
	}

	// 2. La función debe devolver la medida de la diagonal del rectángulo
	// pasado como parámetro. Usar la función distancia() del punto anterior
	// sobre determinados puntos del rectángulo.
	public static double diagonal(Rectangle r) {
		return 0;
	}

	// 3. La función debe devolver el punto central del rectangulo `r`.
	public static Point centro(Rectangle r) {
		return null;
	}

	// 4. La función debe indicar si el punto p está dentro del
	// rectángulo `r`.
	public static boolean estaDentro(Point p, Rectangle r) {
		return false;
	}

	// 5. La función devuelve un nuevo punto que se encuentra en el medio
	// del segmento que une a `p1` con `p2`. Matemáticamente, este punto tiene
	// como coordenada `x` el promedio de las coordenadas `x` de los dos
	// puntos, y lo mismo para la coordenada `y`.
	public static Point puntoMedio(Point p1, Point p2) {
		return null;
	}

	// 6. La función debe devolver el rectángulo más pequeño que contenga a
	// los dos rectángulos pasados como parámetros.
	public static Rectangle encuadrar(Rectangle r1, Rectangle r2) {
		return null;
	}

	// 7. La función debe indicar si el rectangulo `r1` está contenido
	// completamente dentro del rectángulo `r2`.
	public static boolean estaContenido(Rectangle r1, Rectangle r2) {
		return false;
	}

	// 8. (Ejercicio desafío) La función debe devolver el rectángulo contenido
	// en ambos rectángulos `r1` y `r2`. En caso de que no se intersequen debe
	// devolver `null`.
	public static Rectangle interseccion(Rectangle r1, Rectangle r2) {
		return null;
	}

	public static void main(String[] args) {
		int[] a = new int[4];
		String s = "Cristina";
		Point p1 = new Point(3, 4);
	}

}
